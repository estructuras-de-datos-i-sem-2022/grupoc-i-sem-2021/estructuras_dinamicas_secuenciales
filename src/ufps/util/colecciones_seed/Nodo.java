/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ufps.util.colecciones_seed;

import java.util.Objects;

/**
 * Clase que modela un nodo para una lista simple
 * @author madarme
 */
class Nodo<T> {
    
    private T info;
    private Nodo<T> sig;

    protected Nodo() {
    }

    protected Nodo(T info, Nodo<T> sig) {
        this.info = info;
        this.sig = sig;
    }

    protected T getInfo() {
        return info;
    }

    protected void setInfo(T info) {
        this.info = info;
    }

    protected Nodo<T> getSig() {
        return sig;
    }

    protected void setSig(Nodo<T> sig) {
        this.sig = sig;
    }

    @Override
    public String toString() {
        return "Nodo{" + "info=" + info + ", sig=" + sig + '}';
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 67 * hash + Objects.hashCode(this.info);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Nodo<?> other = (Nodo<?>) obj;
        if (!Objects.equals(this.info, other.info)) {
            return false;
        }
        return true;
    }
    
    
    
    
}
