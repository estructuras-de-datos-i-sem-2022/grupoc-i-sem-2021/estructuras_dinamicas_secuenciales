/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Negocio;

import ufps.util.colecciones_seed.ListaCD;

/**
 * SOLO VA USAR ITERADORES PARA RESOLVER LOS MÉTODOS
 * @author madar
 */
public class SistemaNumerico {
    
    private ListaCD<Integer> myLista1;
    private ListaCD<Integer> myLista2;

    public SistemaNumerico() {
    }
    
    public SistemaNumerico(int tam_lista1, int tam_lista2) {
        
        this.myLista1=this.crearLista(tam_lista1);
        this.myLista1=this.crearLista(tam_lista2);
        
    }
    
    
    public ListaCD<Integer> getUnion()
    {
    
        //:)
        return null;
    }
    
    
    
    public ListaCD<Integer> getInterseccion()
    {
    
        //:)
        return null;
    }
    
    
    
    public ListaCD<Integer> getDiferencia()
    {
    
        //:)
        return null;
    }
    
    
    public ListaCD<Integer> getDiferenciaAsimetrica()
    {
    
        //:)
        return null;
    }
    
    
    
    /**
     * 
     * @param n inicio
     * @param m fin
     * @return 
     */
    private ListaCD<Integer> crearListaRandom(int n, int m) {
        ListaCD<Integer> l = new ListaCD();
        while (n-- > 0) {
            //a partir de: http://chuwiki.chuidiang.org/index.php?title=Generar_n%C3%BAmeros_aleatorios_en_Java
            int valorDado = (int)(Math.floor(Math.random()*(n-m+1)+m));
            l.insertarInicio(valorDado);
        }
        return l;
    }
    
    private ListaCD<Integer> crearLista(int n) {
        ListaCD<Integer> l = new ListaCD();
        while (n-- > 0) {
            //a partir de: http://chuwiki.chuidiang.org/index.php?title=Generar_n%C3%BAmeros_aleatorios_en_Java
             l.insertarInicio(n);
        }
        return l;
    }
    
    
}
